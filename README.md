terse
====

**NOTE:** Later, this document may be rewritten in LaTeX, but for now, it'll be in Markdown.

*terse* is a very terse, stack-based, concatenative language built for iOS systems (specifically iOS 7.0+).  Out of the box, the language will also provide graphics capability, allowing the user to write very small programs that can manipulate a canvas.  The aim of the project is to provide a programming language that does not require much writing to execute certain things.  Programming on iOS platforms is practically non-existant, though this is probably for the better.  While terse is a programming language itself, its use is not in writing practical applications, but instead in writing quick programs.

terse is built using Swift 2.x, while the graphics librarry accompanying it is {CoreGraphics?/GLKit?}.  All information regarding the details of terse are explained in the documentation below.

# Documentation
## Stack-based languages
As mentioned in the intro, terse is a stack-based language, meaning that all programs are just manipulations on a stack structure (also known as a LIFO-queue).

At its core, a stack has associated with it two primary operations: push and pop.  Pushing something simply puts it on the top of the stack.  Below is an example of pushing 3 literals, `1`, `2`, and `3`, to the stack:

```
1 2 3
```

The example doesn't really explain much at first.  It should be noted that in nearly every stack-based language, if a literal is read as it is in the example above, it is inferred that that literal is pushed to the stack.  It might help to visualize it like this:

```
push 1
push 2
push 3
```

The expression, `1 2 3`, is equivalent to the one above.  The expression creates a stack where `3` is at the top, followed by `2`, and then `1`.  Now that things are on the stack, it's time to manipulate them with pop.

Pop is essentially just the opposite of push, where pop take the topmost element of the stack.  In the previous example, `1 2 3`, having pop called on it would return `1 2`, where the `3` at the top was removed.  This is almost all there is to pop.  Later on it will be explained how pop plays a role in the actual language.

## terse as a stack-based language
So far, the stack does not seem very useful at all.  Elements can be pushed and popped from it, but ultimately that does nothing outside of the stack.  This is where functions (or words, as called in some stack languages) play in.

Before, it was discussed that the push behavior was inferred from just writing the literals to-be-pushed, as it was in `1 2 3`.  Pop is also inferred, but in a different way.  So far, literals have been covered, but functions haven't yet.  Consider the binary function for integer addition, `+`.  Like most languages, this functions takes to integer arguments and adds them.  The following is the representation of `1 + 2` in terse:

```
1 2 +
```

Here, the literals `1` and `2` are pushed, but upon reading `+`, the two literals are popped.  In general, popping occurs as a way to collect parameters for each function.  Right now, the amount of arguments a function takes will be specified by its stack-effect.  The stack-effect of `+` would be `( a b -- c )`, where `a` and `b` are the parameters, and `c` is the sum.  The stack-effect acts as a way to show what items are popped and what is pushed back when done.  Here, `1` and `2` would be popped and `3` would be pushed in the end.  This approach can allow the user to easily manipulate and concatenate expressions without much hassle:

```
1 2 + 3 *      ~ this is equivalent to 3(1+2).
```

The next section explains how to create functions in terse.

## Functions and quotes
Before functions are covered, it should be noted what quotes are and how they work.  Quotes are essentially literals that represent expressions.  They function the same as lambda expressions in that they can be referred to as "first-class functions".  An example of a quote is below:

```
[ 1 + }
```

The above literal is pushed to the stack as a whole.  Essentially, the user can call the quote to execute its contents.  Given that the call function is represented by `!`, here is an example of this:

```
2 [ 1 + } !
```

What this does is that it pushes `2` and `[ 1 + }` to the stack.  `!` calls the quotation at the top of the stack, essentially making the new expression `2 1 +`, and calling it, returning `3`.  Effectively, `!` takes the contents of the quotation and pushes them to the stack.  Because its a literal, quotations can be used anywhere, even as parameters to functions.  Speaking of functions, it is time to discuss them and their relation to quotations.  In a way, functions are simply an alias for an associated quotation.  For example, here is a way to define a function that takes the average of two numbers:

```
: avg + 2 / }
```

The first token read, `:`, is technically not a function in the same sense `+` and `/` are.  Instead, `:` changes the reading mode of the language to simply read up until a `}`, and then store what is read.  Where this is stored is in the second token read, in this case `avg`.  As expected, `avg` can now be used like any other functions: `1 3 avg`.